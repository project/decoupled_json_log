<?php

declare(strict_types=1);

namespace Drupal\Tests\jsonapi_links\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test module installation and uninstallation.
 *
 * @group decoupled_json_log
 */
class DecoupledJsonLogInstallTest extends KernelTestBase {

  private const string MODULE_NAME = 'decoupled_json_log';

  /**
   * Test that the module can be installed and uninstalled.
   */
  public function testInstallUninstall(): void {
    $this->container->get('module_installer')->install([self::MODULE_NAME]);
    $this::assertSame(0, $this->config('core.extension')->get('module.' . self::MODULE_NAME));

    $this->container->get('module_installer')->uninstall([self::MODULE_NAME]);
    $this::assertNotSame(0, $this->config('core.extension')->get('module.' . self::MODULE_NAME));

    // Try installing and uninstalling again.
    $this->container->get('module_installer')->install([self::MODULE_NAME]);
    $this::assertSame(0, $this->config('core.extension')->get('module.' . self::MODULE_NAME));

    $this->container->get('module_installer')->uninstall([self::MODULE_NAME]);
    $this::assertNotSame(0, $this->config('core.extension')->get('module.' . self::MODULE_NAME));
  }

}
