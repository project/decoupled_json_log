# Decoupled JSON Log module

## Overview

This module provides a JSON Log entity type (machine name: log_json) to help
developers log frontend errors to Drupal.

This is especially useful for decoupled apps, like mobile apps that use a
frontend  framework like Ionic with Drupal for the backend.

Sure, you could use a third-party service, but then you have to put your users'
privacy at risk, plus you usually have to pay for it.

If you're already using Drupal, why not use that to collect logs, too?

## Dependencies

This module stores log entries as JSON, so it uses the
[JSON Field module](https://www.drupal.org/project/json_field).

## Post-Installation

Go to `/admin/people/permissions` and give the `Create json logs` permission to
all roles that should be able to create logs.

Determine what you want to log. By default, this module provides an `error`
bundle for logging errors. You can add other log types at
`/admin/structure/log_json_types`.

Go to `/admin/config/decoupled_json_log` and confirm the rate limit settings.

By default, the anonymous users can log 500 events a day, while authenticated
users can log up to 50 events each. This is done to prevent too many logs from
being created and causing your Drupal site to experience a DDoS attack from your
own app if the frontend code is buggy.

## Assumptions

This module assumes you want to let users log events but limit access to
managing those logged events to admins.

For this reason, you can only POST log entities via REST and JSON:API;
you cannot GET, PATCH, or DELETE them. If you object to this open a feature
request and I will make it configurable.

## Sample TS Code

You can create log entries in the standard way with JSON:API.

```typescript
  /**
   * Stringifies an Error object, since error.toString() doesn't.
   *
   * https://stackoverflow.com/a/30604852
   */
  const stringifyError = (error: Error) =>
      JSON.stringify(error, [
        'message',
        'arguments',
        'type',
        'name',
        'cause',
      ]);

  // Use a function in your frontend to get device info.
  // For example, capacitor has a nice function:
  // https://capacitorjs.com/docs/apis/device#getinfo
  const deviceInfo = await Device.getInfo();

  // Log the error with axios.
  axiosInstance({
    url: 'https://www.example.com/jsonapi/log_json--error',
    data: {
      data: {
        type: 'error',
        attributes: {
          label: 'FrontendFatalError',
          log: stringifyError(error),
          device_info: deviceInfo,
        }
      }
    },
    headers: {
      Accept: application/vnd.api+json,
      'Content-Type': application/vnd.api+json,
      'X-CSRF-Token': 'you_must_get_csrf_token_from_drupal',
    },
    method: 'POST',
  })
```
