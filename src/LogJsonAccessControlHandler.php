<?php

declare(strict_types=1);

namespace Drupal\decoupled_json_log;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the json log entity type.
 *
 * phpcs:disable Drupal.Arrays.Array.LongLineDeclaration
 *
 * @see https://www.drupal.org/project/coder/issues/3185082
 */
final class LogJsonAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  #[\Override]
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResult {
    return match($operation) {
      'view' => AccessResult::allowedIfHasPermissions($account, ['view log_json', 'administer log_json types'], 'OR'),
      'update' => AccessResult::allowedIfHasPermissions($account, ['edit log_json', 'administer log_json types'], 'OR'),
      'delete' => AccessResult::allowedIfHasPermissions($account, ['delete log_json', 'administer log_json types'], 'OR'),
      default => AccessResult::neutral(),
    };
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResult {
    return AccessResult::allowedIfHasPermissions($account, ['create log_json', 'administer log_json types'], 'OR');
  }

}
