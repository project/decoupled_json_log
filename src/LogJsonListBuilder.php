<?php

declare(strict_types=1);

namespace Drupal\decoupled_json_log;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Provides a list controller for the json log entity type.
 */
final class LogJsonListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function buildHeader(): array {
    $header = [];
    $header['id'] = $this->t('ID');
    $header['label'] = $this->t('Label');
    $header['uid'] = $this->t('Author');
    $header['created'] = $this->t('Created');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function buildRow(EntityInterface $entity): array {
    $row = [];
    /** @var \Drupal\decoupled_json_log\LogJsonInterface $entity */
    $row['id'] = $entity->id();
    $row['label'] = $entity->toLink();
    /** @var \Drupal\user\UserInterface $user_entity */
    $user_entity = $entity->get('uid')->entity;
    $username_options = [
      'label' => 'hidden',
      'settings' => ['link' => $user_entity->isAuthenticated()],
    ];
    $row['uid']['data'] = $entity->get('uid')->view($username_options);
    $row['created']['data'] = $entity->get('created')->view(['label' => 'hidden']);
    return $row + parent::buildRow($entity);
  }

}
