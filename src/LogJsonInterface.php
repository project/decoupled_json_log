<?php

declare(strict_types=1);

namespace Drupal\decoupled_json_log;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a json log entity type.
 */
interface LogJsonInterface extends ContentEntityInterface, EntityOwnerInterface {

}
