<?php

declare(strict_types=1);

namespace Drupal\decoupled_json_log;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of json log type entities.
 *
 * @see \Drupal\decoupled_json_log\Entity\LogJsonType
 */
final class LogJsonTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function buildHeader(): array {
    $header = [];
    $header['label'] = $this->t('Label');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function buildRow(EntityInterface $entity): array {
    $row = [];
    $row['label'] = $entity->label();
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function render(): array {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'No json log types available. <a href=":link">Add json log type</a>.',
      [':link' => Url::fromRoute('entity.log_json_type.add_form')->toString()],
    );

    return $build;
  }

}
