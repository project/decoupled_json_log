<?php

declare(strict_types=1);

namespace Drupal\decoupled_json_log\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the JSON Log type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "log_json_type",
 *   label = @Translation("JSON Log type"),
 *   label_collection = @Translation("JSON Log types"),
 *   label_singular = @Translation("json log type"),
 *   label_plural = @Translation("json logs types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count json logs type",
 *     plural = "@count json logs types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\decoupled_json_log\Form\LogJsonTypeForm",
 *       "edit" = "Drupal\decoupled_json_log\Form\LogJsonTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\decoupled_json_log\LogJsonTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer log_json types",
 *   bundle_of = "log_json",
 *   config_prefix = "log_json_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/log_json_types/add",
 *     "edit-form" = "/admin/structure/log_json_types/manage/{log_json_type}",
 *     "delete-form" = "/admin/structure/log_json_types/manage/{log_json_type}/delete",
 *     "collection" = "/admin/structure/log_json_types",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   },
 * )
 */
final class LogJsonType extends ConfigEntityBundleBase {

  /**
   * The machine name of this json log type.
   */
  protected string $id;

  /**
   * The human-readable name of the json log type.
   */
  protected string $label;

}
