<?php

declare(strict_types=1);

namespace Drupal\decoupled_json_log\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\decoupled_json_log\LogJsonInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the json log entity class.
 *
 * @ContentEntityType(
 *   id = "log_json",
 *   label = @Translation("JSON Log"),
 *   label_collection = @Translation("JSON Logs"),
 *   label_singular = @Translation("json log"),
 *   label_plural = @Translation("json logs"),
 *   label_count = @PluralTranslation(
 *     singular = "@count json logs",
 *     plural = "@count json logs",
 *   ),
 *   bundle_label = @Translation("JSON Log type"),
 *   handlers = {
 *     "list_builder" = "Drupal\decoupled_json_log\LogJsonListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\decoupled_json_log\LogJsonAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\decoupled_json_log\Form\LogJsonForm",
 *       "edit" = "Drupal\decoupled_json_log\Form\LogJsonForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "log_json",
 *   admin_permission = "administer log_json types",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "bundle",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/log-json",
 *     "add-form" = "/log-json/add/{log_json_type}",
 *     "add-page" = "/log-json/add",
 *     "canonical" = "/log-json/{log_json}",
 *     "edit-form" = "/log-json/{log_json}/edit",
 *     "delete-form" = "/log-json/{log_json}/delete",
 *     "delete-multiple-form" = "/admin/content/log-json/delete-multiple",
 *   },
 *   constraints = {
 *     "RateLimitPerUser" = {}
 *   },
 *   bundle_entity_type = "log_json_type",
 *   field_ui_base_route = "entity.log_json_type.edit_form",
 * )
 */
final class LogJson extends ContentEntityBase implements LogJsonInterface {

  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);
    if ($this->getOwnerId() === NULL) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(self::class . '::getDefaultEntityOwner')
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the json log was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['log'] = BaseFieldDefinition::create('json')
      ->setLabel(t('Log entry'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'hidden',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['device_info'] = BaseFieldDefinition::create('json')
      ->setLabel(t('Created device'))
      ->setDescription(t('Tracked information about the device that created the log entry.'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'hidden',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
