<?php

declare(strict_types=1);

namespace Drupal\decoupled_json_log\Exception;

/**
 * Exceptions for the Decoupled JSON Log module.
 */
class DecoupledJsonLogException extends \Exception {
}
