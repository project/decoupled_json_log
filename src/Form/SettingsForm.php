<?php

declare(strict_types=1);

namespace Drupal\decoupled_json_log\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Module settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getFormId(): string {
    return 'decoupled_json_log_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('decoupled_json_log.settings');

    $form['rate_limit']['count_anon'] = [
      '#type' => 'number',
      '#title' => $this->t('Count for anonymous users', [], ['context' => 'Decoupled JSON Log']),
      '#description' => $this->t('The maximum number of log entries the anonymous user can create during the interval. This is usually higher than the count for authenticated users because there are many people using the anonymous user account.', [], ['context' => 'Decoupled JSON Log']),
      '#required' => TRUE,
      '#default_value' => $config->get('rate_limit.count_anon'),
    ];

    $form['rate_limit']['count_auth'] = [
      '#type' => 'number',
      '#title' => $this->t('Count for authenticated users', [], ['context' => 'Decoupled JSON Log']),
      '#required' => TRUE,
      '#default_value' => $config->get('rate_limit.count_auth'),
    ];

    $form['rate_limit']['interval_seconds'] = [
      '#type' => 'number',
      '#title' => $this->t('Interval (in seconds)', [], ['context' => 'Decoupled JSON Log']),
      '#description' => $this->t('To get the post count, we will check the range of "now - interval".', [], ['context' => 'Decoupled JSON Log']),
      '#required' => TRUE,
      '#default_value' => $config->get('rate_limit.interval_seconds'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $rate_limit = [
      'count_anon' => $form_state->getValue('count_anon'),
      'count_auth' => $form_state->getValue('count_auth'),
      'interval_seconds' => $form_state->getValue('interval_seconds'),
    ];

    $this->config('decoupled_json_log.settings')
      ->set('rate_limit', $rate_limit)
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  protected function getEditableConfigNames(): array {
    return [
      'decoupled_json_log.settings',
    ];
  }

}
