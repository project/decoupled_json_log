<?php

declare(strict_types=1);

namespace Drupal\decoupled_json_log\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the user has not exceed the rate limit.
 *
 * @Constraint(
 *   id = "RateLimitPerUser",
 *   label = @Translation("Rate Limit Per User", context = "Validation"),
 *   type = "string"
 * )
 */
class RateLimitPerUser extends Constraint {
  /**
   * The message that will be shown if the user has exceeding the post limit.
   *
   * @var string
   */
  public string $postIntervalExceeded = 'Too many entries too soon. Try again later.';

}
