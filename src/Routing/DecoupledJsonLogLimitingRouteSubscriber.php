<?php

declare(strict_types=1);

namespace Drupal\decoupled_json_log\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class DecoupledJsonLogLimitingRouteSubscriber.
 *
 * Do not allow any logs to be listed, edited, or deleted by JSON:API.
 */
class DecoupledJsonLogLimitingRouteSubscriber extends RouteSubscriberBase {

  /**
   * The machine name of entities created by this module.
   */
  private const string ENTITY_MACHINE_NAME = 'log_json';

  /**
   * {@inheritdoc}
   *
   * For details, see
   * https://www.drupal.org/docs/core-modules-and-themes/core-modules/jsonapi-module/security-considerations#s-6-limit-which-entity-bundles-may-be-created-or-editedby-removing-some-routes.
   */
  #[\Override]
  protected function alterRoutes(RouteCollection $collection): void {
    foreach ($collection as $name => $route) {
      $defaults = $route->getDefaults();
      /* @phpstan-ignore-next-line no empty */
      if (!empty($defaults['_is_jsonapi']) && !empty($defaults['resource_type'])) {
        $methods = $route->getMethods();

        if (str_starts_with($name, 'jsonapi.' . self::ENTITY_MACHINE_NAME)) {
          if (
            in_array('PATCH', $methods, TRUE) ||
            in_array('DELETE', $methods, TRUE)
          ) {
            $collection->remove($name);
          }
          elseif (in_array('GET', $methods, TRUE)) {
            $route->setRequirement('_permission', 'administer log_json type');
          }
        }
      }
    }
  }

}
